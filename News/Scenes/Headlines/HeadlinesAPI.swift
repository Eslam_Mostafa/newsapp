//
//  HeadlinesAPI.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import Foundation
import Alamofire

enum HeadlinesAPI : URLRequestConvertible {
    case top_headlines(country: String, category: String, searchKey: String)
}

extension HeadlinesAPI: ApiRouter {
    var path: String {
        switch self {
            
        case .top_headlines:
            return "top-headlines"
        }
    }

    var method: HTTPMethod {
        switch self {
            
        case .top_headlines:
            return .get
        }
    }

    var parameters: Parameters? {
        switch self {
            
        case .top_headlines(let country, let category, let searchKey):
            return [
                Constants.Parameters.COUNTRY : country,
                Constants.Parameters.CATEGORY : category,
                Constants.Parameters.APIKEY : Constants.APIKEY,
                Constants.Parameters.SEARCHKEY : searchKey
            ]
        }
    }
}
