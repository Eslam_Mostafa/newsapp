//
//  HeadlinesViewController.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import UIKit
import RxSwift
import RxCocoa

class HeadlinesViewController: UIViewController, Loadable, UIScrollViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var headlinesTableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var openSearchButton: UIButton!
    @IBOutlet weak var changeLanguageButton: UIButton!
    @IBOutlet weak var headlinesTitleLabel: UILabel!
    @IBOutlet weak var newsAppTitleLabel: UILabel!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let viewModel: HeadlinesViewViewModelProtocol

    // MARK: - Initialization
    init(with viewModel: HeadlinesViewViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setupHeadliesTable()
        configureBinding()
        viewModel.getHeadlinesApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUi()
    }
    
    // MARK: - Methods
    private func setLocalization() {
        searchTextField.text = LocalizedKeys.search
        headlinesTitleLabel.text = LocalizedKeys.headlines
        newsAppTitleLabel.text = LocalizedKeys.newsApp
    }
    
    private func setupUi() {
        self.topView.isHidden = self.viewModel.getTopViewHidenState()
        self.navigationController?.isNavigationBarHidden = !self.topView.isHidden
    }
    
    private func setupHeadliesTable() {
        headlinesTableView.rx.setDelegate(self).disposed(by: disposeBag)
        headlinesTableView.register(HeadLinesTableViewCell.nib, forCellReuseIdentifier: HeadLinesTableViewCell.identifier)
    }
    
    private func configureBinding() {
        viewModel.showProgress
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { [weak self]  in
                guard let self = self else { return }
                self.showLoading(show: $0)
            }).disposed(by: disposeBag)
        
        viewModel.error.subscribe(onNext: { [weak self] error in
            guard let self = self else { return }
            self.showAlert(alertTitle: LocalizedKeys.somethingWrong, alertMessge: error.localizedDescription, actionTitle: LocalizedKeys.cancel)
        }).disposed(by: disposeBag)
        
        viewModel.headlinesSubject.bind(to: headlinesTableView.rx.items(cellIdentifier: HeadLinesTableViewCell.identifier, cellType: HeadLinesTableViewCell.self)) { _, articleModel, cell in
            cell.setupCell(with: articleModel)
        }.disposed(by: disposeBag)
        
        headlinesTableView.rx.modelSelected(Articles.self).subscribe(onNext: { [weak self] model in
            guard let self = self else { return }
            self.openUrlOnBrowser(stringUrl: model.url ?? "")
        }).disposed(by: disposeBag)
        
        openSearchButton.rx.tap
            .subscribe(onNext: {[weak self] _ in
                guard let self = self else { return }
                self.viewModel.navigateToSearchVC()
            }).disposed(by: disposeBag)
        
        changeLanguageButton.rx.tap
            .subscribe(onNext: {[weak self] _ in
                guard let self = self else { return }
                self.changeLanguage()
            }).disposed(by: disposeBag)
    }
    
    private func openUrlOnBrowser(stringUrl: String) {
        if let url = URL(string: stringUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    private func changeLanguage() {
        let changeLangAlert = UIAlertController(title: LocalizedKeys.changeLanguage, message: "".localiz(), preferredStyle: .actionSheet)
        
        changeLangAlert.addAction(UIAlertAction(title: LocalizedKeys.arabic, style: .default, handler: { action in
            if (Language.currentLanguage() == "ar") {
                return
            }
            
            Language.setAppLanguage(language: "ar")
           
            self.viewModel.resetTheApp()
        }))
        
        changeLangAlert.addAction(UIAlertAction(title: LocalizedKeys.english, style: .default, handler: { action in
            
            if(Language.currentLanguage() == "en") {
                return
            }
            
            Language.setAppLanguage(language: "en")
           
            self.viewModel.resetTheApp()
        }))
        
        changeLangAlert.addAction(UIAlertAction(title: LocalizedKeys.cancel, style: .cancel, handler: nil))
        
        self.present(changeLangAlert, animated: true, completion: nil)
    }
}
