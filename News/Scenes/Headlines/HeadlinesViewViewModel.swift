//
//  HeadlinesViewViewModel.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol HeadlinesViewViewModelProtocol {
    var dispatchGroup : DispatchGroup { get }
    var showProgress: PublishSubject<Bool> { get }
    var headlinesSubject : BehaviorRelay<[Articles]> { get }
    var error: PublishSubject<Error> { get }
    func getHeadlinesApi ()
    func getCategories() -> [String]
    func getTopViewHidenState() -> Bool
    func navigateToSearchVC()
    func resetTheApp()
}

class HeadlinesViewViewModel : HeadlinesViewViewModelProtocol {
    
    private let disposeBag = DisposeBag()
    private let headLinesApi: ApiClientProtocol
    private var headlineList : [Articles] = []
    var error = PublishSubject<Error>()
    private var searchList : [String] = []
    private var isFromSearch : Bool = false
    private var searchKey = ""
    
    var showProgress = PublishSubject<Bool>()
    
    var headlinesSubject =  BehaviorRelay<[Articles]>(value: [])
    var dispatchGroup = DispatchGroup()
    
    // MARK: - Initialization
    init(isFromSearch: Bool, searchList: [String] , searchKey: String, headLinesApi: ApiClientProtocol = ApiClient()) {
        self.headLinesApi = headLinesApi
        self.isFromSearch = isFromSearch
        self.searchList = searchList
        self.searchKey = searchKey
    }

    func getCategories() -> [String] {
        if isFromSearch {
            return searchList
        } else {
            return RealmDBHelper.getFavoriteCategories().map({ $0.Name.lowercased() })
        }
    }
    
    func getDateObjectTo(dateString: String ,format: String) -> Date {
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = format
        let result = formatter.date(from: dateString)
        
        return result ?? Date()
    }
    
    func sortHeadlinesByDate() -> [Articles] {
        let sortedResult = self.headlineList.sorted(by: {
             self.getDateObjectTo(dateString: $0.publishedAt ?? "", format: "yyyy-MM-dd'T'HH:mm:ssZ") > self.getDateObjectTo(dateString: $1.publishedAt ?? "", format: "yyyy-MM-dd'T'HH:mm:ssZ")
         })
        
        return sortedResult
    }
    
    func navigateToSearchVC() {
        try? AppNavigator().push(.searchHeadlines)
    }
    
    func resetTheApp() {
        try? AppNavigator().setNewRoot(with: .headLines(isFromSearch: false, searchList: [], searchKey: ""))
    }
    
    func getTopViewHidenState() -> Bool {
        return isFromSearch ? true : false
    }
    
    func getHeadlinesApi() {
        let categoreis = self.getCategories()
        headlineList.removeAll()
        showProgress.onNext(true)
    
        var counter = 0
        
        repeat {
            dispatchGroup.enter()
            let category = (categoreis.count > 0) ? categoreis[counter] : ""
            
            self.headLinesApi.request(HeadlinesAPI.top_headlines(country: CachingManager.shared.get(value: .countryCode) ?? "", category: category, searchKey: self.searchKey), decodingType: HeadlinesModel.self)
                .observe(on: MainScheduler.instance)
                .subscribe(onNext: { response in
                    self.headlineList.append(contentsOf: response.articles ?? [])
                    self.dispatchGroup.leave()
                }, onError: { error in
                    self.error.onNext(error)
                    self.dispatchGroup.leave()
                })
                .disposed(by: disposeBag)
            counter += 1
        } while (counter < categoreis.count)
        
        dispatchGroup.notify(queue: .main) {
            self.headlinesSubject.accept(self.sortHeadlinesByDate())
            self.showProgress.onNext(false)
        }
    }
}
