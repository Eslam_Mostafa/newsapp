//
//  HeadLinesTableViewCell.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import UIKit

class HeadLinesTableViewCell: UITableViewCell {

    @IBOutlet weak var headlineImageView: UIImageView!
    @IBOutlet weak var titlaLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    static var identifier: String {
        return String.init(describing: self)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: String.init(describing: self), bundle: nil)
    }
    
    func setupCell(with dataModel: Articles) {
        headlineImageView.loadFromUrl(stringUrl: dataModel.urlToImage ?? "")
        headlineImageView.cornerRounded()
        titlaLabel.text = dataModel.title ?? ""
        descriptionLabel.text = dataModel.description ?? ""
        sourceLabel.text = LocalizedKeys.source + (dataModel.source?.name ?? "")
        dateLabel.text = dataModel.publishedAt ?? ""
    }
}
