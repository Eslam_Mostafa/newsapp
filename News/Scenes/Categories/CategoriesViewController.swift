//
//  CategoriesViewController.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import UIKit
import RxSwift
import RxCocoa

class CategoriesViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private var categoriesCollectionView: UICollectionView!
    @IBOutlet private var remainingItemCountUILabel: UILabel!
    @IBOutlet weak var chooseFavoriteCategoryLabel: UILabel!
    @IBOutlet weak var welcomToTitleLabel: UILabel!
    @IBOutlet weak var ourNewsAppLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let viewModel: CategoriesViewModelProtocol

    // MARK: - Initialization
    init(with viewModel: CategoriesViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setupCategoriesCollectionC()
        configureBinding()
        viewModel.getCategoriesList()
    }

    // MARK: - Methods
    private func setLocalization() {
        nextButton.setTitle(LocalizedKeys.netx, for: .normal)
        welcomToTitleLabel.text = LocalizedKeys.welcomeTo
        ourNewsAppLabel.text = LocalizedKeys.ourNewsApp
        chooseFavoriteCategoryLabel.text = LocalizedKeys.chooseFavoritesCategories
    }
    
    private func setupCategoriesCollectionC() {
        categoriesCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        categoriesCollectionView.register(CategoryCollectionViewCell.nib, forCellWithReuseIdentifier: CategoryCollectionViewCell.identifier)
    }
    
    private func configureBinding() {
        viewModel.categoriesListSubject.subscribe(onNext: { [weak self] categoryList in
            guard let self = self else { return }
            self.remainingItemCountUILabel.text = "( \( 3 - categoryList.filter({$0.isSelected}).count) " +  LocalizedKeys.remaining + " )"
            self.enableNextButton(enable: categoryList.filter({$0.isSelected}).count == 3)
        }).disposed(by: disposeBag)
        
        viewModel.categoriesListSubject.bind(to: categoriesCollectionView.rx.items(cellIdentifier: CategoryCollectionViewCell.identifier, cellType: CategoryCollectionViewCell.self)) { _, categoryModel, cell in
            cell.setupCell(with: categoryModel)
        }.disposed(by: disposeBag)
        
        categoriesCollectionView.rx.itemSelected.subscribe(onNext: { [weak self] index in
            self?.viewModel.selectStatus(for: index.row)
        }).disposed(by: disposeBag)
        
        nextButton.rx.tap
            .subscribe(onNext: {[weak self] _ in
                guard let self = self else { return }
                self.viewModel.saveFavoriteCategories()
                self.viewModel.navigateToHeadlinesVC()
            }).disposed(by: disposeBag)
    }
    
    private func enableNextButton(enable: Bool) {
        if enable {
            nextButton.alpha = 1
            nextButton.isUserInteractionEnabled = true
        } else {
            nextButton.alpha = 0.3
            nextButton.isUserInteractionEnabled = false
        }
    }
}

// MARK: - CollectionView Delegate Methods
extension CategoriesViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (categoriesCollectionView.bounds.width) / 2
        return CGSize(width: width, height: 64)
    }
}
