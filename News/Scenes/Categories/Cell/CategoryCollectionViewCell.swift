//
//  CategoryCollectionViewCell.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static var identifier: String {
        return String.init(describing: self)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: String.init(describing: self), bundle: nil)
    }
    
    func setupCell(with dataModel: CategoriesUIModel) {
        categoryLabel.text = dataModel.categoryName
        categoryImageView.image = UIImage.init(named: dataModel.categoryImageName)
        containerView.layer.cornerRadius = 16
        
        if dataModel.isSelected {
            categoryImageView.tintColor = PublicMethods.hexaStringToUIColor("#3AC139")
            containerView.backgroundColor = PublicMethods.hexaStringToUIColor("#DDFFDD")
            containerView.layer.borderColor = PublicMethods.hexaStringToUIColor("#3AC139").cgColor
            containerView.layer.borderWidth = 1.2
            categoryLabel.textColor = PublicMethods.hexaStringToUIColor("#3AC139")
        } else {
            categoryImageView.tintColor = PublicMethods.hexaStringToUIColor("#575252")
            containerView.backgroundColor = PublicMethods.hexaStringToUIColor("#F5F7FB")
            containerView.layer.borderWidth = 0
            categoryLabel.textColor = PublicMethods.hexaStringToUIColor("#575252")
        }
    }
}

struct CategoriesUIModel {
    let categoryName: String
    let categoryImageName: String
    var isSelected: Bool = false
}
