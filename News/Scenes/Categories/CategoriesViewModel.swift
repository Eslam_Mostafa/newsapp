//
//  CategoriesViewModel.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol CategoriesViewModelProtocol {
    var categoriesListSubject: BehaviorRelay<[CategoriesUIModel]> { get }
    func getCategoriesList()
    func selectStatus(for index: Int)
    func saveFavoriteCategories()
    func navigateToHeadlinesVC()
}

class CategoriesViewModel: CategoriesViewModelProtocol {
   
    var categoriesListSubject = BehaviorRelay<[CategoriesUIModel]>(value: [])
    
    func getCategoriesList() {
        let categoriesList = [ CategoriesUIModel.init(categoryName: "Business", categoryImageName: "ic_business"),
                               CategoriesUIModel.init(categoryName: "General", categoryImageName: "ic_general"),
                               CategoriesUIModel.init(categoryName: "Entertainment", categoryImageName: "ic_entertainment"),
                               CategoriesUIModel.init(categoryName: "Health", categoryImageName: "ic_health"),
                               CategoriesUIModel.init(categoryName: "Science", categoryImageName: "ic_science"),
                               CategoriesUIModel.init(categoryName: "Sports", categoryImageName: "ic_sports"),
                               CategoriesUIModel.init(categoryName: "Technology", categoryImageName: "ic_technology")
        ]
        categoriesListSubject.accept(categoriesList)
    }
    
    func selectStatus(for index: Int) {
        var categoriesList = categoriesListSubject.value
        if categoriesList.filter({$0.isSelected}).count < 3 ||  categoriesList[index].isSelected == true {
            categoriesList[index].isSelected =  !categoriesList[index].isSelected
            categoriesListSubject.accept(categoriesList)
        }
    }
    
    func saveFavoriteCategories() {
        RealmDBHelper.saveFavoriteCategories(categoriesListSubject.value.filter({ $0.isSelected == true }))
    }
    
    func navigateToHeadlinesVC() {
        try? AppNavigator().push(.headLines(isFromSearch: false, searchList: [], searchKey: ""))
    }
    
}
