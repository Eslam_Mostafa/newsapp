//
//  SearchHeadlinesViewModel.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol SearchHeadlinesViewModelProtocol {
    var categoriesListSubject: BehaviorRelay<[CategoriesUIModel]> { get }
    func getCategoriesList()
    func selectStatus(for index: Int)
    func navigateToSearchResult(_ searchKey : String)
    func getFavoriteCategores() -> [FavoriteCategories]
}

class SearchHeadlinesViewModel: SearchHeadlinesViewModelProtocol {
    
    var categoriesListSubject = BehaviorRelay<[CategoriesUIModel]>(value: [])
    
    func getCategoriesList() {
        let favoritecategoriesList = getFavoriteCategores()
        var categoriesList = [ CategoriesUIModel.init(categoryName: "Business", categoryImageName: "ic_business"),
                               CategoriesUIModel.init(categoryName: "General", categoryImageName: "ic_general"),
                               CategoriesUIModel.init(categoryName: "Entertainment", categoryImageName: "ic_entertainment"),
                               CategoriesUIModel.init(categoryName: "Health", categoryImageName: "ic_health"),
                               CategoriesUIModel.init(categoryName: "Science", categoryImageName: "ic_science"),
                               CategoriesUIModel.init(categoryName: "Sports", categoryImageName: "ic_sports"),
                               CategoriesUIModel.init(categoryName: "Technology", categoryImageName: "ic_technology")
        ]
        
        categoriesList = categoriesList.map{
            
            var mutualObj = $0
            
            if let _ = favoritecategoriesList.filter({ $0.Name == mutualObj.categoryName }).first{
                mutualObj.isSelected = true
            }
            
            return mutualObj
        }
        
        categoriesListSubject.accept(categoriesList)
    }
    
    func selectStatus(for index: Int) {
        var categoriesList = categoriesListSubject.value
        categoriesList[index].isSelected =  !categoriesList[index].isSelected
        categoriesListSubject.accept(categoriesList)
    }
    
    func navigateToSearchResult(_ searchKey : String) {
        var selectedCategoriesList = categoriesListSubject.value.filter{ $0.isSelected == true}.map({ $0.categoryName.lowercased() })
        
        if selectedCategoriesList.count == categoriesListSubject.value.count{
            selectedCategoriesList = []
        }
        
        try? AppNavigator().push(.headLines(isFromSearch: true, searchList: selectedCategoriesList, searchKey: searchKey))
    }
    
    func getFavoriteCategores() -> [FavoriteCategories] {
        return RealmDBHelper.getFavoriteCategories()
    }
}
