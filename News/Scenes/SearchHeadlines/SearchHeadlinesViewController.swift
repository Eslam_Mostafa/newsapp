//
//  SearchHeadlinesViewController.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import UIKit
import RxSwift
import RxCocoa

class SearchHeadlinesViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet private var categoriesCollectionView: UICollectionView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let viewModel: SearchHeadlinesViewModelProtocol
    private var searchBar = UISearchBar()
    
    // MARK: - Initialization
    init(with viewModel: SearchHeadlinesViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
        setupSearchBar()
        setupCategoriesCollection()
        configureBinding()
        viewModel.getCategoriesList()
    }
    
    // MARK: - Methods
    func setupUi() {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func setupCategoriesCollection() {
        categoriesCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        categoriesCollectionView.register(CategoryCollectionViewCell.nib, forCellWithReuseIdentifier: CategoryCollectionViewCell.identifier)
    }
    
    private func configureBinding() {
        viewModel.categoriesListSubject.bind(to: categoriesCollectionView.rx.items(cellIdentifier: CategoryCollectionViewCell.identifier, cellType: CategoryCollectionViewCell.self)) { _, categoryModel, cell in
            cell.setupCell(with: categoryModel)
        }.disposed(by: disposeBag)
        
        categoriesCollectionView.rx.itemSelected.subscribe(onNext: { [weak self] index in
            self?.viewModel.selectStatus(for: index.row)
        }).disposed(by: disposeBag)
    }
    
    private func setupSearchBar() {
        searchBar.becomeFirstResponder()
        searchBar.searchBarStyle = .default
        searchBar.placeholder = LocalizedKeys.search
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        navigationItem.titleView = searchBar
    }
    
}

// MARK: - CollectionView Delegate Methods
extension SearchHeadlinesViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (categoriesCollectionView.bounds.width) / 2
        return CGSize(width: width, height: 64)
    }
}

//MARK: - Search bar delegate
extension SearchHeadlinesViewController : UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let searchKey = searchBar.text ?? ""
        
        viewModel.navigateToSearchResult(searchKey)
    }
}
