//
//  CountryCollectionViewCell.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import UIKit

class CountryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var countryNameLabel: UILabel!
    
    static var identifier: String {
        return String.init(describing: self)
    }
    
    static var nib: UINib {
        return UINib.init(nibName: String.init(describing: self), bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(country: Country) {
        countryNameLabel.text = country.name ?? ""
        containerView.layer.cornerRadius = 16
        
        if country.isSelected {
            containerView.backgroundColor = PublicMethods.hexaStringToUIColor("#DDFFDD")
            containerView.layer.borderColor = PublicMethods.hexaStringToUIColor("#3AC139").cgColor
            containerView.layer.borderWidth = 1.2
            countryNameLabel.textColor = PublicMethods.hexaStringToUIColor("#3AC139")
        } else {
            containerView.backgroundColor = PublicMethods.hexaStringToUIColor("#F5F7FB")
            containerView.layer.borderWidth = 0
            countryNameLabel.textColor = .black
        }
    }
}
