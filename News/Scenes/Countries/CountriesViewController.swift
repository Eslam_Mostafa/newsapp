//
//  CountriesViewController.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import UIKit
import RxSwift
import RxCocoa

class CountriesViewController: UIViewController {
    // MARK: - Outlet
    @IBOutlet weak var welcomToTitleLabel: UILabel!
    @IBOutlet weak var ourNewsAppLabel: UILabel!
    @IBOutlet weak var countriesCollectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var chooseCountryLabel: UILabel!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let viewModel: CountriesViewModelProtocol

    // MARK: - Initialization
    init(with viewModel: CountriesViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        configureBinding()
        setUpCollectionView()
        viewModel.getCountries()
    }
    
    // MARK: - Methods
    private func setLocalization() {
        nextButton.setTitle(LocalizedKeys.netx, for: .normal)
        welcomToTitleLabel.text = LocalizedKeys.welcomeTo
        ourNewsAppLabel.text = LocalizedKeys.ourNewsApp
        chooseCountryLabel.text = LocalizedKeys.chooseCountry
    }
    
    private func setUpCollectionView() {
        countriesCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        countriesCollectionView.register(CountryCollectionViewCell.nib, forCellWithReuseIdentifier: CountryCollectionViewCell.identifier)
    }
    
    private func configureBinding() {
        viewModel.countriesListSubject.subscribe(onNext: { [weak self] countryList in
            guard let self = self else { return }
            self.enableNextButton(enable: countryList.filter({$0.isSelected}).count > 0)
        }).disposed(by: disposeBag)
        
        viewModel.countriesListSubject.bind(to: countriesCollectionView.rx.items(cellIdentifier: CountryCollectionViewCell.identifier, cellType: CountryCollectionViewCell.self)) { _, countryModel, cell in
            cell.setupCell(country: countryModel)
        }.disposed(by: disposeBag)
        
        countriesCollectionView.rx.itemSelected.subscribe(onNext: { [weak self] index in
            guard let self = self else { return }
            self.viewModel.selectStatus(for: index.item)
            self.enableNextButton(enable: true)
        }).disposed(by: disposeBag)
        
        nextButton.rx.tap
            .subscribe(onNext: {[weak self] _ in
                guard let self = self else { return }
                self.viewModel.saveSelectedCountry()
                self.viewModel.navigateToCategoriesVC()
            }).disposed(by: disposeBag)
    }
    
    private func enableNextButton(enable: Bool) {
        if enable {
            nextButton.alpha = 1
            nextButton.isUserInteractionEnabled = true
        } else {
            nextButton.alpha = 0.3
            nextButton.isUserInteractionEnabled = false
        }
    }
}

//MARK:- UICollectionView Methods
extension CountriesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (countriesCollectionView.bounds.width) / 2
        return CGSize(width: width, height: 64)
    }
}
