//
//  CountriesModel.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import Foundation

struct CountriesModel : Codable {
    let data : [Country]?

    enum CodingKeys: String, CodingKey {

        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent([Country].self, forKey: .data)
    }
}

struct Country : Codable {
    let name : String?
    let shortName : String?
    let middleName : String?
    let code : String?
    let imageUrl : String?
    let countryId : String?
    var isSelected = false

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case shortName = "shortName"
        case middleName = "middleName"
        case code = "code"
        case imageUrl = "imageUrl"
        case countryId = "countryId"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        shortName = try values.decodeIfPresent(String.self, forKey: .shortName)
        middleName = try values.decodeIfPresent(String.self, forKey: .middleName)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
        countryId = try values.decodeIfPresent(String.self, forKey: .countryId)
    }

}
