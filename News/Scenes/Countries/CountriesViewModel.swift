//
//  CountriesViewModel.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import Foundation
import RxSwift
import RxCocoa

protocol CountriesViewModelProtocol {
    var countriesListSubject: BehaviorRelay<[Country]> { get }
    func getCountries()
    func selectStatus(for index: Int)
    func saveSelectedCountry()
    func navigateToCategoriesVC()
}

class CountriesViewModel : CountriesViewModelProtocol {
    var countriesListSubject = BehaviorRelay<[Country]>(value: [])
    
    func getCountries() {
        self.fetchDataFromJSON(sourceFileName: "Countries", decodingType: CountriesModel.self) { data in
            self.countriesListSubject.accept(data.data ?? [])
        }
    }
    
    func fetchDataFromJSON<T: Decodable>(sourceFileName: String,decodingType: T.Type,completionHandler: @escaping (_ data: T) -> Void) {

        if let url = Bundle.main.url(forResource: sourceFileName, withExtension: "json") {
            let result = try! Data(contentsOf: url)
            do {
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode(decodingType.self, from: result)
                completionHandler(responseModel)
                
            } catch(let error) {
                print(error.localizedDescription)
            }
        }
    }
    
    func selectStatus(for index: Int) {
        var countryList = countriesListSubject.value
        
        if countryList.filter({$0.isSelected}).count < 1 {
            countryList[index].isSelected.toggle()
            countriesListSubject.accept(countryList)
        } else if !countryList[index].isSelected {
            countryList.indices.forEach {
                countryList[$0].isSelected = false
            }
            
            countryList[index].isSelected.toggle()
            countriesListSubject.accept(countryList)
        }
    }
    
    func saveSelectedCountry() {
        if let country = countriesListSubject.value.filter({$0.isSelected == true}).first {
            CachingManager.shared.set((country.shortName ?? "").lowercased(), with: .countryCode)
            print(CachingManager.shared.getCached(value: .countryCode) ?? "")
        }
    }
    
    func navigateToCategoriesVC() {
        try? AppNavigator().push(.categories)
    }
}
