//
//  CashingManager.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import Foundation

final class CachingManager {
    
    static let shared = CachingManager()
    let cach = UserDefaults.standard
    
    private init() {}
    
    func cache<T: Encodable>(_ obj: T, with key: CachKeys) -> Bool {
        guard let data = try? JSONEncoder().encode(obj) else {return false}
        cach.setValue(data, forKey: key.rawValue)
        cach.synchronize()
        return true
    }
    
    func clear(with key: CachKeys) {
        cach.setValue(nil, forKey: key.rawValue)
        cach.synchronize()
    }
    
    func set(_ obj: Any, with key: CachKeys) {
        cach.setValue(obj, forKey: key.rawValue)
        cach.synchronize()
    }
    
    func get<T>(value of: CachKeys) -> T? {
        return cach.value(forKey: of.rawValue) as? T
    }
    
    func getCached<T: Decodable>(value of: CachKeys) -> T? {
        if let data = cach.value(forKey: of.rawValue) as? Data {
            let obj = try? JSONDecoder().decode(T.self, from: data)
            return obj
        }
        return nil
    }
    
    func resetApp() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
    }
}

enum CachKeys: String {
    case countryCode
    case firstLaunch
    case languageId
}
