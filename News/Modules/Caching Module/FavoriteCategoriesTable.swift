//
//  FavoriteCategoriesTable.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import Foundation
import RealmSwift

class FavoriteCategories: Object {
    @objc dynamic var Id = ""
    @objc dynamic var Name = ""
}
