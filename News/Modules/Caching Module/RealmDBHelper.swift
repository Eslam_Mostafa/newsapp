//
//  RealmDBHelper.swift
//  News
//
//  Created by Eslam Mostafa on 08/02/2022.
//

import Foundation
import RealmSwift

class RealmDBHelper : NSObject {
    
    class func ifMigrationNeeded() {
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
    }
    
    class func saveFavoriteCategories(_ list: [CategoriesUIModel]) {
        ifMigrationNeeded()
        let realm = try! Realm()
        let favorites = realm.objects(FavoriteCategories.self)
        
        try! realm.write {
            
            if  favorites.count != 0 {
                realm.delete(favorites)
            }
            
            for i in 0 ..< list.count {
                let category  = FavoriteCategories()
                category.Id = ""
                category.Name = list[i].categoryName
                realm.add(category)
            }
        }
    }
    
    class func getFavoriteCategories() -> [FavoriteCategories]{
        let realm = try! Realm()
        let favoriteList = realm.objects(FavoriteCategories.self)
        return Array(favoriteList)
    }
}
