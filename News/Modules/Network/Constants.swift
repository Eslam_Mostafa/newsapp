//
//  Constants.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import Foundation
struct Constants {
    //The API's base URL
    static let BASEURL = "https://newsapi.org/v2/"
    static let APIKEY = "e44a1d1b74024e73842beb4381f04776"
    
    //The parameters (Queries) that we're gonna use
    struct Parameters {
        static let APIKEY = "apiKey"
        static let COUNTRY = "country"
        static let CATEGORY = "category"
        static let SEARCHKEY = "q"
        static let LANGUAGE = "language"
    }
    
    //The header fields
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    //The content type (JSON)
    enum ContentType: String {
        case json = "application/json"
    }
}
