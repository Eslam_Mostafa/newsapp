//
//  Destinations.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import UIKit

enum Destinations {
    case countries
    case categories
    case headLines(isFromSearch : Bool, searchList : [String] , searchKey: String)
    case searchHeadlines
    
    var viewcontroller: UIViewController {
        switch self {
    
        case .countries:
            let viewModel = CountriesViewModel()
            return CountriesViewController(with: viewModel)
        case .categories:
            let viewModel = CategoriesViewModel()
            return CategoriesViewController(with: viewModel)
        case .headLines(let isFromSearch, let searchList , let searchKey):
            let viewModel = HeadlinesViewViewModel(isFromSearch: isFromSearch, searchList: searchList, searchKey: searchKey)
            return HeadlinesViewController(with: viewModel)
        case .searchHeadlines:
            let viewModel = SearchHeadlinesViewModel()
            return SearchHeadlinesViewController(with: viewModel)
        }
    }
}
