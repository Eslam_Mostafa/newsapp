//
//  LocalizedKeys.swift
//  News
//
//  Created by Eslam Mostafa on 09/02/2022.
//

import Foundation


struct LocalizedKeys {
    static var netx: String {
        return "Next".localiz()
    }
    
    static var changeLanguage: String {
        return "Change Language".localiz()
    }
    
    static var arabic: String {
        return "Arabic".localiz()
    }
    
    static var english: String {
        return "English".localiz()
    }
    
    static var cancel: String {
        return "Cancel".localiz()
    }
    
    static var search: String {
        return "Search".localiz()
    }
    
    static var headlines: String {
        return "Headlines".localiz()
    }
    
    static var newsApp: String {
        return "NewsApp".localiz()
    }
    
    static var somethingWrong : String {
        return "Something wrong!".localiz()
    }
    
    static var source: String {
        return "Source: ".localiz()
    }
    
    static var welcomeTo: String {
        return "Welcome to".localiz()
    }
    
    static var ourNewsApp: String {
        return "our News App".localiz()
    }
    
    static var chooseCountry: String {
        return "Choose your country".localiz()
    }
    
    static var chooseFavoritesCategories: String {
        return "Choose your favorite categories".localiz()
    }
    
    static var remaining: String {
        return "Remaining".localiz()
    }
}
