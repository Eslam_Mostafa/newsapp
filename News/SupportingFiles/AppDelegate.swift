//
//  AppDelegate.swift
//  News
//
//  Created by Eslam Mostafa on 07/02/2022.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        setupMainViewController(window ?? UIWindow())
        setupKeyboardManager()
        
        return true
    }

    private func setupMainViewController(_ window:UIWindow) {
        
        var viewController = Destinations.countries.viewcontroller
        
        if (CachingManager.shared.get(value: .countryCode) ?? "") != "" && RealmDBHelper.getFavoriteCategories().count > 0 {
            viewController = Destinations.headLines(isFromSearch: false, searchList: [], searchKey: "").viewcontroller
        } else {
            changeToSystemLanguage()
        }
        
        _ = AppNavigator(window: window, viewController: viewController)
    }
    
    private func setupKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    //MARK:- make app language same as system lang in first lanuch and call once after first install
    private func changeToSystemLanguage() {
        let preferredLanguage = NSLocale.preferredLanguages[0]
        
        if preferredLanguage.contains("ar") {
            CachingManager.shared.set("1", with: .languageId)
            Language.setAppLanguage(language: "ar")
        } else {
            CachingManager.shared.set("2", with: .languageId)
            Language.setAppLanguage(language: "en")
        }
    }
    
}

